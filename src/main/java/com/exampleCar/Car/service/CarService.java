package com.exampleCar.Car.service;

import com.exampleCar.Car.model.Car;

import java.util.List;

public interface CarService {
    Car get(Integer id);
    List<Car> getAll();
    Car create(Car car);
    Car update(Integer id, Car car);
    void delete(Integer id);
}

