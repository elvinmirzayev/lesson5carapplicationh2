package com.exampleCar.Car.service;

import com.exampleCar.Car.config.Config;
import com.exampleCar.Car.model.Car;
import com.exampleCar.Car.repository.CarRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class CarServiceImpl implements CarService{
    private final CarRepository carRepository;
    private final Config config;

    public CarServiceImpl(CarRepository carRepository, Config config) {
        this.carRepository = carRepository;
        this.config = config;
    }

    @Override
    public Car get(Integer id) {
        log.info("car service get method");
        System.out.println(config.getList());
        Optional<Car> car = carRepository.findById(id);
        if (car.isEmpty()){
            throw new RuntimeException("Car not found");
        }
        return car.get();
    }


    @Override
    public List<Car> getAll() {
        log.info("car service getAll method");
        List<Car> cars = carRepository.findAll();
        if (cars.isEmpty()){
            throw new RuntimeException("Cars not found");
        }
        return cars;
    }

    @Override
    public Car create(Car car) {
        log.info("car service create method");
        car.setColor(config.getColors().get(0));
        return carRepository.save(car);
    }

    @Override
    public Car update(Integer id, Car car) {
        log.info("car service update method");
        Car entity = carRepository.findById(id).orElseThrow(() -> new RuntimeException());
        entity.setModel(car.getModel());
        entity.setYears(car.getYears());
        entity.setColor(car.getColor());
        entity.setPrice(car.getPrice());
        entity.setCondition(car.getCondition());
        return entity;
    }

    @Override
    public void delete(Integer id) {
        log.info("car service delete method");
        carRepository.deleteById(id);
    }

}

